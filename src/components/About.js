import {Container, Row, Col} from 'react-bootstrap';

export default function About() {

	return (

		<Container className="bg-secondary p-2">
			<Row>
				<Col className="mx-5 my-1">
					<h1 className="my-5">About Me</h1>
					<h2 className="mt-3">Carlo Tagudin</h2>
					<h3 className="">FullStack Web Dev</h3>
					<p className="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
					<h4>Contact Me:</h4>
					<ul>
						<li>Email: admin123@gmail.com</li>
						<li>Mobile: 8008569420</li>
						<li>Address: Metro Manila, Philippines</li>
					</ul>
				</Col>
			</Row>
		</Container>

	)

}