import {useState, useEffect, useContext} from 'react';
import {Table, Button} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import UserContext from '../userContext';
import ModalComponent from './Modal';


export default function AdminDashboard() {

	const {user} = useContext(UserContext);

	const [allCourses, setAllCourses] = useState([]);

	function archive(courseId) {

		//console.log(courseId);
		fetch(`http://localhost:4000/courses/archive/${courseId}`, {

			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			window.location.href="/courses";
		})
	}

	function activate(courseId) {

		//console.log(courseId);
		fetch(`http://localhost:4000/courses/activate/${courseId}`, {

			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			window.location.href="/courses";
		})
	}


	function editCourse(courseId) {

		console.log(courseId);


	}

	useEffect(() => {

		if (user.isAdmin){

			fetch('http://localhost:4000/courses/', {
				headers: {
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				setAllCourses(data.map(course => {

					return (

						<tr key={course._id}>
							<td>{course._id}</td>
							<td>{course.name}</td>
							<td>{course.price}</td>
							<td>{course.isActive ? "Active" : "Inactive"}</td>
							<td className="text-center">
								<Button variant="warning" className="mx-2" onClick={() => {editCourse(course._id)}} >Edit</Button>
								{
									course.isActive
									? 
									<Button variant="danger" className="mx-2" onClick={() => {archive(course._id)}} >Archive</Button>
									:
									<Button variant="success" className="mx-2" onClick={() => {activate(course._id)}} >Activate</Button>
								}
							</td>
						</tr>

						)

				}))
			})

		}

	}, [])


	return (

		<>
			<h1 className="my-5 text-center" >Admin Dashboard</h1>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Price</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>idSample</td>
						<td>nameSample</td>
						<td>priceSample</td>
						<td>statusSample</td>
						<td>actionSample</td>
					</tr>
						{allCourses}
				</tbody>
			</Table>
		</>
	)

}