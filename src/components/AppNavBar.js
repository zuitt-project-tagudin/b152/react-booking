import {useContext} from 'react';

import {Nav, Navbar, Container} from 'react-bootstrap';

import UserContext from '../userContext';

export default function AppNavBar() {

	const {user} = useContext(UserContext);


	return (

		<Navbar bg="secondary" expand="lg">
			<Container fluid>
				<Navbar.Brand href="#home">B152 Booking</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ml-auto">
						<Nav.Link href="/">Home</Nav.Link>
						<Nav.Link href="/courses">Courses</Nav.Link>
						{
						    user.id
						    ? 
						    	user.isAdmin
						    	?
						    	<>						    		
						    		<Nav.Link href="/addCourse">AddCourse</Nav.Link>
						    		<Nav.Link href="/logout">Logout</Nav.Link>
						    	</>
						    	:
						    	<Nav.Link href="/logout">Logout</Nav.Link>
						    :
						    <>
								<Nav.Link href="/login">Login</Nav.Link>
								<Nav.Link href="/register">Register</Nav.Link>
						    </>
						}
						
					</Nav>
				</Navbar.Collapse>				
			</Container>
		</Navbar>

		)


}