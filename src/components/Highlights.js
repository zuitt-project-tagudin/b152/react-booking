import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights({highlightProp}) {



	return (

		<Row className="my-3">
			<Col xs={12} md={4}>
				<Card className="p-3 cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2>Learn From Home</h2>
						</Card.Title>
						<Card.Text>
							In amet officia cupidatat proident aliquip dolore cillum sint aliqua sed laboris dolore enim excepteur dolor incididunt consectetur ex esse laborum consectetur elit dolor sint sed tempor irure.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="p-3 cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum ullamco ex culpa aliquip occaecat fugiat consectetur aliquip non in sed amet ut sint consequat veniam commodo aliqua commodo ad consequat aliquip nulla veniam.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="p-3 cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2>Be Part of Our Community</h2>
						</Card.Title>
						<Card.Text>
							In amet officia cupidatat proident aliquip dolore cillum sint aliqua sed laboris dolore enim excepteur dolor incididunt consectetur ex esse laborum consectetur elit dolor sint sed tempor irure.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>

		)

}