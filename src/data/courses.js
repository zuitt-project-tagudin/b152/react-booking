let coursesData = [

	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Sed nulla irure anim voluptate est dolore do esse duis sed dolor elit ea nostrud commodo reprehenderit dolore dolor.",
		price: 25000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python-Django",
		description: "Nulla dolor et duis aute quis culpa consectetur elit ex do anim laboris dolore laboris ut labore et.",
		price: 35000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Eu minim irure in et sunt do consequat in aliqua laborum culpa est duis sint commodo esse..",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc004",
		name: "Lorem-Ipsum",
		description: "Lorem ipsum dolore ut non veniam ut ut dolore sed id veniam commodo sunt incididunt in ullamco elit.",
		price: 30000,
		onOffer: false
	},

]

export default coursesData;