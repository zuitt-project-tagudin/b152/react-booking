import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import 'bootstrap/dist/css/bootstrap.min.css'



//let element = <h1>Hello, World!</h1>

/*let myName = 
  <>
    <h2>Peter Porker</h2>
    <h3>Dev</h3>
  </>

let name = "Jeffrey Lacdao";
let job = "Professional Driver";

let personDisplay1 = (
<>
    <h1>{name}</h1>
    <h2>{job}</h2>
</>
  )

let num1 = 150;
let num2 = 250

let numDisplay = (

  <>
    <h4>The sum of {num1} and {num2} is :</h4>
    <p>{num1 + num2}</p>
  </>

  )

let person = {

  name: "Stephen Strange",
  age: 45, 
  job: "Sorcerer Supreme",
  income: 50000,
  expense: 30000,
  address: "New York City, New York"

}

let personDisplay2 = <p>Hi my name is {person.name}. I am {person.age} years old. I live in {person.address}</p>

let personDisplay3 = (
  <>
    <h1>Name: {person.name}</h1>
    <h2>Job: {person.job}</h2>
    <h3>Age: {person.age}</h3>
    <h3>Address: {person.address}</h3>
    <h4>Income: $  {person.income}</h4>
    <h4>Expense: $  {person.expense}</h4>
    <h4>Balance: $  {person.income - person.expense}</h4>
  </>
  )


const SampleComp = () => {

  return (

      <h1>I am a react element returned by a function</h1>

    )

}*/

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
