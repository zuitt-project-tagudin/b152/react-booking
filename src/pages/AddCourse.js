import {useState} from 'react';
import {Form, Button} from 'react-bootstrap';

import Swal from 'sweetalert2';

export default function CreateCourse(e) {

	const[name, setName] = useState("");
	const[description, setDesc] = useState("");
	const[price, setPrice] = useState("");

	function createCourse(e) {

		e.preventDefault();

		console.log(name);
		console.log(description);
		console.log(price);

		let token =localStorage.getItem('token');

		fetch('http://localhost:4000/courses/', {

			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data._id) {

				Swal.fire({
					icon: "success",
					title: "Course Creation Successful",
				})
			} else {

				Swal.fire({
					icon: "error",
					title: "Failed",
					text: data.message
				})

			}

		})

	}

	return (

		<>
			<h1 className="my-5 text-center" >Add Course</h1>
			<Form onSubmit={e => createCourse(e)}>
				<Form.Group>
					<Form.Label>Name:</Form.Label>
					<Form.Control type="text" required value={name} onChange={e => {setName(e.target.value)}} />
				</Form.Group>
				<Form.Group>
					<Form.Label>Description:</Form.Label>
					<Form.Control type="text" required value={description} onChange={e => {setDesc(e.target.value)}} />
				</Form.Group>
				<Form.Group>
					<Form.Label>Price:</Form.Label>
					<Form.Control type="number" required value={price} onChange={e => {setPrice(e.target.value)}} />
				</Form.Group>
				<Button varaiant="primary" type="submit" className="my-5" >Add Course</Button>
			</Form>
		</>

	)

}