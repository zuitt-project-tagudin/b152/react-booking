import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {

/*	let sampleProp = "I am sample data passed from Home component to Banner component."*/

	let sampleProp2 = "This sample data is passed from Home to Highlights"

	let bannerData = {

		title: "Zuitt Booking System B152",
		description: "View and book a course from our catalog",
		buttonText: "View our Courses!",
		destination: "/courses"

	}

	return (

			<>
				<Banner bannerProp={bannerData} />
				<Highlights highlightProp={sampleProp2} />
			</>

		)

}